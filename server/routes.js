/**
 * Main application routes
 */

import errors from './components/errors';
import path from 'path';
//Lab04 step 4.1.1
import * as users from './api/users';

export default function(app) {

    //Lab04 Step 4.2.1
    app.use('/api/users', users.router);

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|auth|components|app|bower_components|assets)/*')
        .get(errors[404]);

    // All other routes should redirect to the app.html
    app.route('/*')
        .get((req, res) => {
            res.sendFile(path.resolve(`${app.get('appPath')}/app.html`));
        });
}
