import express from 'express';

import * as users from './users';

let router = express.Router();

//Lab04
router.get('/', users.listContents);
router.get('/:id', users.findOne);
router.post('/', users.createUser);

//Lab05
router.put('/:id', users.updateUser);
router.delete('/:id', users.removeUser);

export {router}; //whoever imports index.js will get the export of index.js which is the Router object
