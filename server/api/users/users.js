import uuidv4 from 'uuid/v4';

//initialize users array
let users = [];

//Lab04: returns list of all users
export function listContents(req, res)
{
  /* Implementation here */
  res.status(200);
  res.json({
    users: users
  });
}

//helper function to search user array for given ID
//given by Dr. Pittman w/ Lab04 answers
function findUser(id)
{
  let foundUsers = users.filter(function(user)
  {
    if(user.id === id)
    {
      return true;
    }
    return false;
  });

  if(foundUsers.length > 0)
  {
    return foundUsers[0];
  }
  else
  {
    return null;
  }
}

//Lab04: finds user with specified ID
//corrected using Pittman's code
export function findOne(req, res)
{
  let existingUser = findUser(req.params.id);

  if(existingUser)
  {
    res.status(200);
    res.json(existingUser);
  }
  else
  {
    res.status(404);
    res.json({message: 'Not Found'});
  }

}

//Lab04: creates new user
  //corrected using Pittman's code
export function createUser(req, res)
{
  let id = uuidv4();

  //fill user attributes
  let name = req.body.name;
  let address = req.body.address;
  let age = req.body.age;

  //create user
  let user =
    {
      id,
      name,
      address,
      age
    };

  //add user to users array
  users.push(user);

  //set status code to 201
  res.status(201);
  //respond w/ user object and message created
  res.json(user);
}

//Lab05
export function updateUser(req, res)
{
  let existingUser = findUser(req.params.id);
  if(existingUser)
  {
    existingUser.name = req.body.name;
    existingUser.address = req.body.address;
    existingUser.age = req.body.age;

    res.status(200);
    res.json(existingUser);
  }
  //if user doesn't exist, a new user will be created
    //using the id provided by the req
  else
  {
    createUser(req, res);
  }

}

//Lab05: helper function for delete user
//gets the index of the user within the users[] array
function findUserIndex(id)
{
  let currentIndex = 0;

  for(var i = 0; i < users.length; i++)
  {
    let currentId = users[i].id;

    //if id's don't match, keep looking
    if(id != currentId)
    {
      currentIndex++;
    }

    //return index of id
    else
    {
      return currentIndex;
    }
  }
}

//Lab05
export function removeUser(req, res)
{
  let existingUser = findUser(req.params.id);
  //if user exists
  if(existingUser)
  {
    //find index of existing user
    let index = findUserIndex(existingUser.id);
    //splice user
    users.splice(index);
    res.status(204).send();
  }
  //if no matching user is found
  else
  {
    res.status(404);
    res.json({message: "Not Found"});
  }
}
