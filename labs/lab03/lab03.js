import fs from 'fs';
import zlib from 'zlib';

//Lab03
//Evan O'Rourke and
//Haley Hammond

class lab03 {

syncFileRead(filename)
{
	var data = fs.readFileSync(filename);
	return data.toString();
}

asyncFileRead(filename, callback) {
    fs.readFile(filename, (err, data) => {
      if(err) {
        console.error(err);
      } else {
        return callback(data.toString());
      }
    });
  }


  compressFileStream(infile, outfile)
  {
    let writeStream = fs.createWriteStream(outputFile);
    fs.createReadStream(inputFile)
      .pipe(zlib.createGzip())
      .pipe(writeStream);
    return writeStream;
  }

  decompressFileStream(infile, outfile)
  {
    let writeStream = fs.createWriteStream(outputFile);
    fs.createReadStream(inputFile)
      .pipe(zlib.createGunzip())
      .pipe(writeStream);
    return writeStream;
  }

  //multiple attempts because I can't figure out why there is a
  //"statement expected" paren error
  listDirectoryContents(directory_name, callback) {

  fs.readdir(directoryLocation, (err, files) => {
      if(err) {
        return console.error(err);
      }
      return callback(files);
    });
  }
}

export {lab03};
