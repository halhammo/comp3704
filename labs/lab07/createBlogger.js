//Create a new database named blogger with a collection named articles
// Insert a new article with an author name and email, creation date, and text


//> mongo --host web2-mongodb:27017 blogger
//> use blogger

db.articles.insert(
  {
    authorName: "Mary Shelly",
    email: "MaryShelly@gmail.com",
    creationDate: ISODate("1800-07-01"),
    text: "Frankenstein"
  }
);

